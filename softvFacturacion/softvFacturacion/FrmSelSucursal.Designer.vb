﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelSucursal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.DataGridTmp = New System.Windows.Forms.DataGridView()
        Me.DataGridPro = New System.Windows.Forms.DataGridView()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Clv_Sucursal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sucursal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Sucursal2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sucursal2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridTmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridPro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(351, 141)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 25)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(351, 172)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(64, 25)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(351, 203)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(64, 25)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(351, 234)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(64, 25)
        Me.Button4.TabIndex = 5
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(494, 397)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "&ACEPTAR"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(636, 397)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 7
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'DataGridTmp
        '
        Me.DataGridTmp.AllowUserToAddRows = False
        Me.DataGridTmp.AllowUserToDeleteRows = False
        Me.DataGridTmp.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridTmp.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridTmp.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridTmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridTmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Sucursal2, Me.Sucursal2})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridTmp.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridTmp.Location = New System.Drawing.Point(446, 42)
        Me.DataGridTmp.Name = "DataGridTmp"
        Me.DataGridTmp.ReadOnly = True
        Me.DataGridTmp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridTmp.Size = New System.Drawing.Size(276, 319)
        Me.DataGridTmp.TabIndex = 11
        '
        'DataGridPro
        '
        Me.DataGridPro.AllowUserToAddRows = False
        Me.DataGridPro.AllowUserToDeleteRows = False
        Me.DataGridPro.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridPro.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridPro.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridPro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridPro.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Sucursal, Me.Sucursal})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridPro.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridPro.Location = New System.Drawing.Point(34, 42)
        Me.DataGridPro.Name = "DataGridPro"
        Me.DataGridPro.ReadOnly = True
        Me.DataGridPro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridPro.Size = New System.Drawing.Size(276, 319)
        Me.DataGridPro.TabIndex = 10
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'Clv_Sucursal
        '
        Me.Clv_Sucursal.DataPropertyName = "Clv_Sucursal"
        Me.Clv_Sucursal.HeaderText = "Clv_Sucursal"
        Me.Clv_Sucursal.Name = "Clv_Sucursal"
        Me.Clv_Sucursal.ReadOnly = True
        Me.Clv_Sucursal.Visible = False
        '
        'Sucursal
        '
        Me.Sucursal.DataPropertyName = "Sucursal"
        Me.Sucursal.HeaderText = "Sucursal"
        Me.Sucursal.Name = "Sucursal"
        Me.Sucursal.ReadOnly = True
        Me.Sucursal.Width = 200
        '
        'Clv_Sucursal2
        '
        Me.Clv_Sucursal2.DataPropertyName = "Clv_Sucursal2"
        Me.Clv_Sucursal2.HeaderText = "Clv_Sucursal"
        Me.Clv_Sucursal2.Name = "Clv_Sucursal2"
        Me.Clv_Sucursal2.ReadOnly = True
        Me.Clv_Sucursal2.Visible = False
        '
        'Sucursal2
        '
        Me.Sucursal2.DataPropertyName = "Sucursal2"
        Me.Sucursal2.HeaderText = "Sucursal"
        Me.Sucursal2.Name = "Sucursal2"
        Me.Sucursal2.ReadOnly = True
        Me.Sucursal2.Width = 200
        '
        'FrmSelSucursal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(784, 445)
        Me.Controls.Add(Me.DataGridTmp)
        Me.Controls.Add(Me.DataGridPro)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "FrmSelSucursal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Sucursales"
        CType(Me.DataGridTmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridPro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents DataGridTmp As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridPro As System.Windows.Forms.DataGridView
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Clv_Sucursal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sucursal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Sucursal2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sucursal2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
