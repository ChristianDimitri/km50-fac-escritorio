﻿Public Class FrmRepPromocion

    Private Sub dtFechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtFechaIni.ValueChanged
        dtFechaFin.MinDate = dtFechaIni.Value
    End Sub

    Private Sub dtFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtFechaFin.ValueChanged
        dtFechaIni.MaxDate = dtFechaFin.Value
    End Sub

    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        eFechaIni = dtFechaIni.Value
        eFechaFin = dtFechaFin.Value
        eC = cbC.Checked
        eI = cbI.Checked
        eD = cbD.Checked
        eS = cbS.Checked
        eB = cbB.Checked
        eF = cbF.Checked

        eTitulo = "Reporte de Promociones con status de"
        If cbC.Checked = True Then eTitulo += " Contratado"
        If cbI.Checked = True Then eTitulo += " Instalado"
        If cbD.Checked = True Then eTitulo += " Desconectado"
        If cbS.Checked = True Then eTitulo += " Suspendido"
        If cbB.Checked = True Then eTitulo += " Baja"
        If cbF.Checked = True Then eTitulo += " Fuéra de Área"


        GloReporte = 13
        FrmImprimirRepGral.Show()
        Me.Close()

    End Sub

    Private Sub btCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCancelar.Click
        Me.Close()
    End Sub

    Private Sub FrmRepPromocion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dtFechaIni.Value = Today
        dtFechaFin.Value = Today
    End Sub
End Class